[![CircleCI](https://circleci.com/bb/yameveo/silex-skeleton.svg?style=svg)](https://circleci.com/bb/yameveo/silex-skeleton)

# Silex Skeleton

This is a basic silex skeleton sample, steps to install it:

````bash
$ composer create-project --stability=dev yameveo/silex-skeleton name-of-your-project
$ cd name-of-your-project/
$ make setup PROJECT_NAME=name-of-your-project
````

At this point, the .git folder will be removed, so you can init the project into a new repo.

## Environment pre-setup

Requirements (Mac OS):

* Install [Docker Toolbox](https://www.docker.com/products/docker-toolbox)
* Have a virtual machine named `default`
* Run eval $(docker-machine env)

Requirements (Any Linux):

* Have docker installed

Run `docker-machine ip` and add the ip to your hosts file:

```
192.168.99.100 name-of-your-project.dev
```

## Run environment

```bash
make dev
```

## Kill the environment

```bash
make nodev
```

## See the container logs

```bash
make logs
```

## Enter a container

```bash
make enter

  Write the name of the service that you want to access, possible choices are:

    * phpfpm
    * nging

Service name: phpfpm
root@b20f96fdf431:/app/name-of-your-project#
```

# TESTS

* Run phpspec tests:
```bash
bin/phpspec run
```
* Run behat test:
```bash
bin/behat
```

