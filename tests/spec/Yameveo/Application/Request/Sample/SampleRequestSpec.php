<?php

namespace spec\Yameveo\Application\Request\Sample;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SampleRequestSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(1);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Yameveo\Application\Request\Sample\SampleRequest');
    }
}
