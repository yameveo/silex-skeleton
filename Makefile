COMPONENT := {{@COMPONENT_NAME@}}
CODE_CONTAINER := phpfpm
APP_ROOT := /app/{{@COMPONENT_NAME@}}

all: dev logs

dev:
	@docker-compose -p ${COMPONENT} -f ops/docker/docker-compose.yml up -d
	@sleep 2

enter:
	@./ops/scripts/enter.sh ${COMPONENT}

kill:
	@docker-compose -p ${COMPONENT} -f ops/docker/docker-compose.yml kill

nodev:
	@docker-compose -p ${COMPONENT} -f ops/docker/docker-compose.yml kill
	@docker-compose -p ${COMPONENT} -f ops/docker/docker-compose.yml rm -f
ifeq ($(IMAGES),true)
	@docker rmi ${COMPONENT}_${CODE_CONTAINER}
	@docker rmi ${COMPONENT}_${CLI_CONTAINER}

endif

setup:
	@./ops/scripts/setup.sh ${PROJECT_NAME}

deps: dependencies

dependencies:
	@composer install --no-interaction

restart: nodev dev fixtures logs

logs:
	@docker-compose -p ${COMPONENT} -f ops/docker/docker-compose.yml logs

.PHONY: test docs
