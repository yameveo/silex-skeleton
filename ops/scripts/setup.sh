#!/bin/bash

if [ -z "$1" ]; then
    echo 'Missing PROJECT_NAME parameter';
    exit;
fi

COMPONENT=$1;

sed -i '' -e "s/{{@COMPONENT_NAME@}}/$COMPONENT/g" ./Makefile
sed -i '' -e "s/{{@COMPONENT_NAME@}}/$COMPONENT/g" ./ops/docker/docker-compose.yml
sed -i '' -e "s/{{@COMPONENT_NAME@}}/$COMPONENT/g" ./ops/docker/phpfpm/Dockerfile
sed -i '' -e "s/{{@COMPONENT_NAME@}}/$COMPONENT/g" ./ops/docker/phpfpm/confs/php-fpm-7.conf
sed -i '' -e "s/{{@COMPONENT_NAME@}}/$COMPONENT/g" ./ops/docker/nginx/Dockerfile
sed -i '' -e "s/{{@COMPONENT_NAME@}}/$COMPONENT/g" ./ops/docker/nginx/conf.d/nginx.conf
sed -i '' -e "s/{{@COMPONENT_NAME@}}/$COMPONENT/g" ./ops/docker/nginx/conf.d/site.conf
sed -i '' -e "s/silex-skeleton/$COMPONENT/g" ./composer.json

# Remove license
rm -rf LICENSE.txt