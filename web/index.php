<?php
require __DIR__.'/../vendor/autoload.php';
$app = new Silex\Application();
$app['debug'] = true;
require __DIR__.'/../app/config/config.php';
require __DIR__.'/../src/Yameveo/Infrastructure/Silex/services.php';
require __DIR__.'/../src/Yameveo/Infrastructure/Silex/routes.php';
$app->run();