<?php

$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new \Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../../../../app/logs/dev.log'
));

// Controllers
$app['sample.controller'] = function($app) {
    return new \Yameveo\Infrastructure\Silex\Controller\SampleController($app['sample.use_case']);
};

//Use Cases
$app['sample.use_case'] = function($app) {
    return new \Yameveo\Application\UseCase\Sample\SampleUseCase();
};