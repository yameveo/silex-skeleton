<?php
namespace Yameveo\Infrastructure\Silex\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Yameveo\Application\Request\Sample\SampleRequest;
use Yameveo\Application\UseCase\Sample\SampleUseCase;

class SampleController
{
    /**
     * @var SampleUseCase
     */
    protected $sampleUseCase;

    /**
     * SampleController constructor.
     * @param SampleUseCase $sampleUseCase
     */
    public function __construct(SampleUseCase $sampleUseCase)
    {
        $this->sampleUseCase = $sampleUseCase;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getSampleAction(Request $request)
    {
        return new JsonResponse($this->sampleUseCase->execute(
            new SampleRequest($request->get('value'))
        )->getResponseParam());
    }
}