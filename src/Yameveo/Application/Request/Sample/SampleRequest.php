<?php

namespace Yameveo\Application\Request\Sample;

class SampleRequest
{
    /**
     * @var int
     */
    protected $param;

    /**
     * SampleRequest constructor.
     * @param $param
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * @return int
     */
    public function getParam()
    {
        return $this->param;
    }
}