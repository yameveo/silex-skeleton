<?php
namespace Yameveo\Application\Response\Sample;

class SampleResponse
{
    /**
     * @var int
     */
    protected $responseParam;

    /**
     * SampleResponse constructor.
     * @param $responseParam
     */
    public function __construct($responseParam)
    {
        $this->responseParam = $responseParam;
    }

    /**
     * @return int
     */
    public function getResponseParam()
    {
        return $this->responseParam;
    }
}