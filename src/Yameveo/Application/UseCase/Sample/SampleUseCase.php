<?php
namespace Yameveo\Application\UseCase\Sample;

use Yameveo\Application\Request\Sample\SampleRequest;
use Yameveo\Application\Response\Sample\SampleResponse;

class SampleUseCase
{
    public function __construct()
    {
    }

    public function execute(SampleRequest $request)
    {
        $param = $request->getParam();
        return new SampleResponse($param+10);
    }
}